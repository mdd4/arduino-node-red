const host = "37.97.203.138";
const port = 9001;
const user = "mdd";
const pass = "loislane";
const clientId = "testSanderFrontend";
const topics = [
  "distance",
  "distance/smooth",
  "distance/range",
  "distance/smoothrange",
  "button",
];
let client;
const method = "default";

const confettiSettings = { target: "my-canvas", size: 2 };
let confetti;

function init() {
  println("Connecting to " + host + ":" + port + " as " + clientId);

  client = new Paho.MQTT.Client(host, Number(port), clientId);
  // Set callback handlers
  client.onConnectionLost = onConnectionLost;
  client.onMessageArrived = onMessageArrived;
  // Connect the client, if successful, call onConnect function
  client.connect({
    userName: user,
    password: pass,
    onSuccess: onConnect,
    useSSL: window.location.protocol === "https:",

    onFailure: console.log,
  });
}

// Called when the client connects
function onConnect() {
  println("connected.");
  println("Subscribing to: " + topics.join(", "));

  // Subscribe to the requested topics
  topics.forEach(client.subscribe);
}

function onConnectionLost(responseObject) {
  println("Connection lost");
  if (responseObject.errorCode !== 0) {
    println("ERROR: " + +responseObject.errorMessage);
  }
}

// Called when a message arrives
function onMessageArrived(message) {
  println(`${message.destinationName}: ${message.payloadString}`);

  if (message.destinationName === "distance" && method === "default") {
    setSunSize(message.payloadString);
  }

  if (message.destinationName === "distance/smooth" && method === "smooth") {
    setSunSize(message.payloadString);
  }

  if (message.destinationName === "distance/range" && method === "range") {
    setSmartSunSize(message.payloadString);
  }

  if (
    message.destinationName === "distance/smoothrange" &&
    method === "smoothrange"
  ) {
    setSmartSunSize(message.payloadString);
  }

  if (message.destinationName === "button") {
    if (message.payloadString === "1") {
      confetti = new ConfettiGenerator(confettiSettings);
      confetti.render();
    }

    if (message.payloadString === "0") {
      confetti.clear();
    }
  }
}

function setSunSize(distance) {
  const sun = document.getElementById("sun");

  const maxGrow = 500;
  const maxDistance = 150;
  const minSize = 150;

  const sizePercentage = Math.min(
    100,
    (parseInt(distance) * 100) / maxDistance
  );

  const size = minSize + (1 - sizePercentage / 100) * maxGrow + "px";
  sun.style.width = size;
  sun.style.height = size;
}

function setSmartSunSize(size) {
  const sun = document.getElementById("sun");

  sun.style.width = size + "px";
  sun.style.height = size + "px";
}

function println(msg) {
  const messages = document.getElementById("messages");
  const messagesToShow = 10;

  if (messages.childNodes.length > messagesToShow) {
    messages.childNodes = messages.childNodes.forEach((node, index) => {
      if (index === 0) messages.removeChild(node);
    });
  }

  messages.innerHTML += "<div>" + msg + "</div>";
  messages.scrollTop = messages.scrollHeight;
}

document.addEventListener("DOMContentLoaded", (event) => {
  //the event occurred
  init();
  document.getElementById("code").innerHTML = hardCalc;
});

function selectSunSizeMethod() {
  const value = document.getElementById("options").value;
  method = value;

  if (value.includes("range")) {
    code.innerHTML = rangeCalc;
  } else {
    code.innerHTML = hardCalc;
  }
}

const hardCalc = `
function setSunSize(distance) {
  const sun = document.getElementById("sun");

  const maxGrow = 500;
  const maxDistance = 150;
  const minSize = 150;

  const sizePercentage = Math.min(
    100,
    (parseInt(distance) * 100) / maxDistance
  );

  const size = minSize + (1 - sizePercentage / 100) * maxGrow + "px";
  sun.style.width = size;
  sun.style.height = size;
}
`;

const rangeCalc = `
function setSmartSunSize(size) {
  const sun = document.getElementById("sun");

  sun.style.width = size + "px";
  sun.style.height = size + "px";
}
`;
