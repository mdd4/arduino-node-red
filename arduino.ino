#ifdef ESP32
#include <WiFi.h>
#else
#include <ESP8266WiFi.h>
#endif

#include <PubSubClient.h>

WiFiClient espClient;
PubSubClient client(espClient);

/**
 * Config
 */
// Wifi
const char *ssid = "iotroam";
const char *password = "loislane";

// MQTT
const char *mqtt_server = "37.97.203.138";
const int mqtt_port = 1883;
const char *mqtt_user = "mdd";
const char *mqtt_password = "loislane";
const char *mqtt_identifier = "testSanderArduino";

// Pins
const int pingPin = 26; // A0 Trigger Pin of Ultrasonic Sensor
const int echoPin = 25; // A1 Echo Pin of Ultrasonic Sensor
const int buttonPin = 21;

// State
String temp_str;
char output[50];
int buttonState = 0;

void setup()
{
    Serial.begin(9600);
    Serial.println();

    setup_wifi();
    client.setServer(mqtt_server, mqtt_port);
    client.setCallback(callback);

    pinMode(buttonPin, INPUT_PULLUP);
    pinMode(pingPin, OUTPUT);
    pinMode(echoPin, INPUT);
}

void loop()
{
    if (!client.connected())
    {
        reconnect();
    }
    client.loop();

    long distance = get_distance();

    if (distance < 1500)
    {
        temp_str = String(distance);
        temp_str.toCharArray(output, temp_str.length() + 1);
        client.publish("distance", output);
    }

    if (buttonState != digitalRead(buttonPin))
    {
        Serial.println("new button state");
        buttonState = digitalRead(buttonPin);
        if (buttonState == HIGH)
        {
            client.publish("button", "1");
        }
        if (buttonState == LOW)
        {
            client.publish("button", "0");
        }
    }

    delay(150);
}

void setup_wifi()
{
    Serial.print("ESP Board MAC Address:  ");
    Serial.println(WiFi.macAddress());

    delay(10);
    // We start by connecting to a WiFi network
    Serial.println();
    Serial.print("Connecting to ");
    Serial.println(ssid);

    WiFi.begin(ssid, password);

    while (WiFi.status() != WL_CONNECTED)
    {
        delay(500);
        Serial.print(".");
    }

    Serial.println("");
    Serial.println("WiFi connected");
    Serial.println("IP address: ");
    Serial.println(WiFi.localIP());
}

void callback(char *topic, byte *message, unsigned int length)
{
    Serial.print("Message arrived on topic: ");
    Serial.print(topic);
    Serial.print(". Message: ");
    String messageTemp;

    for (int i = 0; i < length; i++)
    {
        Serial.print((char)message[i]);
        messageTemp += (char)message[i];
    }
    Serial.println();
}

void reconnect()
{
    // Loop until we're reconnected
    while (!client.connected())
    {
        Serial.print("Attempting MQTT connection...");
        // Attempt to connect
        if (client.connect(mqtt_identifier, mqtt_user, mqtt_password))
        {
            Serial.println("connected");
            // Subscribe
            client.subscribe("esp32/output");
        }
        else
        {
            Serial.print("failed, rc=");
            Serial.print(client.state());
            Serial.println(" try again in 5 seconds");
            // Wait 5 seconds before retrying
            delay(5000);
        }
    }
}

long get_distance()
{
    long duration, inches, cm;
    digitalWrite(pingPin, LOW);
    delayMicroseconds(2);
    digitalWrite(pingPin, HIGH);
    delayMicroseconds(10);
    digitalWrite(pingPin, LOW);
    duration = pulseIn(echoPin, HIGH);
    cm = microsecondsToCentimeters(duration);
    Serial.print(cm);
    Serial.print("cm");
    Serial.println();
    return cm;
}

long microsecondsToCentimeters(long microseconds)
{
    return microseconds / 29 / 2;
}